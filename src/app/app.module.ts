import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StarRatingModule } from 'projects/star-rating-lib/src/lib/star-rating-lib.module';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    StarRatingModule,
    ReactiveFormsModule,
    MatIconModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
