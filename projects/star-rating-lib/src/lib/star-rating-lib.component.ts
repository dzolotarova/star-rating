import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export interface StarRating {
  id: number;
  selected: boolean;
}

@Component({
  selector: 'lib-star-rating',
  templateUrl: './star-rating-lib.component.html',
  styleUrls: ['./star-rating-lib.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => StarRatingLibComponent),
    multi: true,
  }],
})
export class StarRatingLibComponent implements ControlValueAccessor {
  disabled = false;
  stars: StarRating[] = [];

  constructor() {
    this.stars = this.generateStars();
  }

  private onChange = (value: any) => {};
  private onTouched = () => {};

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.onTouched = fn;
  }

  writeValue(outsideValue: number): void {
    this.updateStars(outsideValue);
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  updateValue(insideValue: number): void {
    this.updateStars(insideValue);
    this.onChange(insideValue);
    this.onTouched();
  }

  private updateStars(rating: number): void {
    this.stars = this.stars.map((star, index) => {
      if (rating <= index) {
        return {
          ...star,
          selected: false
        };
      }

      return {
        ...star,
        selected: true
      };
    });
  }

  private generateStars(): StarRating[] {
    return [
      {
        id: 1,
        selected: false,
      },
      {
        id: 2,
        selected: false,
      },
      {
        id: 3,
        selected: false,
      },
      {
        id: 4,
        selected: false,
      },
      {
        id: 5,
        selected: false,
      },
    ];
  }
}
