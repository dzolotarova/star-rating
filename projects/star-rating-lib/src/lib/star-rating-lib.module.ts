import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { StarRatingLibComponent } from './star-rating-lib.component';

@NgModule({
  declarations: [StarRatingLibComponent],
  imports: [
    CommonModule,
    MatIconModule,
  ],
  exports: [StarRatingLibComponent]
})
export class StarRatingModule { }
