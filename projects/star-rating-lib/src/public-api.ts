/*
 * Public API Surface of star-rating-lib
 */

export * from './lib/star-rating-lib.component';
export * from './lib/star-rating-lib.module';
